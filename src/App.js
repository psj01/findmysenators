import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import "./App.css";
import NavBar from "./Components/NavBar";
import Senators from "./Components/Senators";
import Representatives from "./Components/Representatives";
import NotFound from "./Components/NotFound";
import Home from "./Components/Home";

function App() {
  return (
    <div className="App">
      <NavBar />
      <Switch>
        <Route path="/senators" component={Senators} />
        <Route path="/representatives/:id" component={Representatives} />
        <Route path="/not-found" component={NotFound} />
        <Route path="/home" component={Home} />
        <Route path="/" exact component={Home} />
        <Redirect to="/not-found" />
      </Switch>
    </div>
  );
}

export default App;
